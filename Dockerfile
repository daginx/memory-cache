FROM node:10-alpine

RUN apk update && apk upgrade && apk add --no-cache bash git

ARG WORK_DIR=/var/www/node
RUN mkdir -p ${WORK_DIR}
WORKDIR ${WORK_DIR}

COPY package*.json ${WORK_DIR}/
RUN npm install

COPY . ${WORK_DIR}/
RUN npm run build
RUN rm -fr node_modules
RUN rm -fr src
RUN npm install --production

ENTRYPOINT  ["npm", "run", "start"]