import { Router } from 'express';

interface IControllerBase {
  router: Router;
  initRoutes(): any;
}

export default IControllerBase;
