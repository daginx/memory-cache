import * as redis from 'redis';
import { promisify } from 'util';
import { Config } from '../../config/config';
const client = redis.createClient({
  host: Config.redis.host,
  port: Config.redis.port,
});

client.on('error', function(error) {
  console.error(error);
});

export default {
  getAsync: promisify(client.get).bind(client),
  setAsync: promisify(client.set).bind(client),
};
