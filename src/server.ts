import App from './app';

import * as bodyParser from 'body-parser';
import loggerMiddleware from './middlewares/logger';

import HomeController from './controllers/home.controller';
import { Config } from './config/config';

const app = new App({
  host: Config.app.host,
  port: Config.app.port,
  controllers: [new HomeController()],
  middleWares: [
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    loggerMiddleware,
  ],
});

app.listen();
