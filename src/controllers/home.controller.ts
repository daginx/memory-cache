import * as express from 'express';
import { Request, Response } from 'express';
import RedisClient from '../shared/cache/redisClient';
import IControllerBase from '../interfaces/IControllerBase.interface';

class HomeController implements IControllerBase {
  public router = express.Router();

  constructor() {
    this.initRoutes();
  }

  public initRoutes() {
    this.router.get('/', this.index);
    this.router.get('/values/:key', this.get);
    this.router.post('/values', this.createOrUpdate);
  }

  index = (req: Request, res: Response) => {
    res.json({ ok: true });
  };

  createOrUpdate = async (req: Request, res: Response) => {
    const { key, value } = req.body;
    const result = await RedisClient.setAsync(key, value);
    res.json({ result });
  };

  get = async (req: Request, res: Response) => {
    const { key } = req.params;
    const value = await RedisClient.getAsync(key);
    res.json({ key, value });
  };
}

export default HomeController;
