export const Config = {
  redis: {
    host: process.env.REDIS_HOST || '127.0.0.1',
    port: Number(process.env.REDIS_PORT) || 6379,
  },
  app: {
    host: process.env.HOST || '127.0.0.1',
    port: Number(process.env.PORT) || 5000,
  },
};
