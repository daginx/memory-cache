# Distributed Memory Cache

## Getting started

```bash
docker-compose up
```

## Testing

```bash
curl --location --request POST 'localhost:5001/values' --header 'Content-Type: application/json' --data-raw '{"key": "test","value": "testvalue"}'

curl 'localhost:5002/values/test'
```
